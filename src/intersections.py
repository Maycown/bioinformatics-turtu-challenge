import pyranges as pr
import pandas as pd
import glob


def join_overlapping(df_a, df_b):
	"""
	Using the `join` method from Pyranges: https://pyranges.readthedocs.io/en/latest/autoapi/pyranges/index.html#pyranges.PyRanges.join
	find all overlapping (identical) positions from two file.
	Args:
		df_a: a list of human genetic variants previously associated with disease predisposition
		df_b: a list of human loss-of-function (LoF) variants

	Returns:
		pd.DataFrame: DataFrame with all the intersections in the range determined by start and end positions.
	"""
	pr_human_associated_disease = pr.PyRanges(df_a)
	pr_human_loss_function = pr.PyRanges(df_b)
	return pr_human_associated_disease.join(pr_human_loss_function, slack=1).as_df()

def save_intersections_per_chromosome(df_intersections, output_path):
	"""Save a tabulated .tsv file to each chromosome in folder `../data/processed/chromosome`.
	Each file contains the columns:
	chromosome, start_position, end_position, variant_id, variant_type, variant_source, variant_alleles, variant_maf

	Args:
		df_intersections (pd.DataFrame): DataFrame with the data of previously calculated intersections
		output_path (str): Path to save the files processed.

	Returns:
		None:

	"""
	columns = ['chromosome', 'start_position', 'end_position', 'variant_id', 'variant_type', 'variant_source',
	           'variant_alleles', 'variant_maf']
	df_intersections = (df_intersections.rename(columns={'Chromosome': 'chromosome',
	                   'Start'     : 'start_position',
	                   'End'       : 'end_position',
	                   'var_source': 'variant_source',
	                   'var_type'  : 'variant_type',
	                   'maf'       : 'variant_maf'
	                   }).drop(columns=['Start_b', 'var_id', 'End_b'], inplace=False))[columns]

	for c in df_intersections['chromosome'].unique():
		df_intersections[df_intersections['chromosome']==c].to_csv(output_path + "/" + str(c) + ".tsv",
		                                                           sep="\t", index=False)


def calculate_maf(folder_path="../data/processed/chromosome", save_file=False,
                  output_path="data/processed"):
	"""Calculates the minimum, average and maximum minor allele frequency (MAF/maf) observed across the LoF variants
	on that chromosome. Print and save the information in folder `../data/processed`.

	Args:
		folder_path (str): Folder path with all the .tsv files generated.
		save_file (bool): If the information of calculated MAF will be save into a file.

	Returns:
		None
	"""
	all_files = glob.glob(folder_path + "/*.tsv")
	li = []

	for filename in all_files:
		df = pd.read_csv(filename, index_col=None, header=0, sep='\t')
		li.append(df)

	frame = pd.concat(li, axis=0, ignore_index=True).groupby(by='chromosome', as_index=False)\
		.agg({'variant_maf': ['min', 'mean', 'max']})
	frame.columns = ['chromosome', 'min_maf', 'average_maf', 'max_maf']
	print(frame)
	if save_file:
		frame.to_csv(output_path+"/chromosome_maf.tsv", sep='\t', index=False)
