import pandas as pd


def read_file(file_path, expected_n_column, sep='\t', *args, **kwargs):
	"""Read the file using Pandas IO reading methods

		Args:
			file_path (:obj:`str`): File path to be opened.
			expected_n_column (int): Number of expected columns of file.
				sep (char): character delimiter used (default='\t')
			*args: Variable length argument list.
			**kwargs: Arbitrary keyword arguments.

		Returns:
			pd.DataFrame: File in python Pandas DataFrame Format.

		Raises:
			ValueError: If the number of columns is not `expected_n_columns`.
	"""
	df = pd.read_csv(file_path, sep=sep)
	n_column = df.shape[1]
	if n_column != expected_n_column:
		raise ValueError(f"The number of the columns should be \
				{expected_n_column}, but {n_column} was found.")
	df.rename(columns={'chr'              : 'Chromosome',
	                   'chromosome'       : 'Chromosome',
	                   'var_start_pos'    : 'Start',
	                   'variant_start_pos': 'Start',
	                   'var_end_pos'      : 'End',
	                   'variant_end_pos'  : 'End'
	                   },
	          inplace=True
	          )

	return df


def read_variants_associated_with_disease_predisposition(file_path, expected_n_column=6, *args, **kwargs):
	"""Read the file containing  a list of human genetic variants
	previously associated with disease predisposition

	Args:
		file_path (:obj:`str`): File path to be opened.
		expected_n_column (int): Number of expected columns of file (default=6).
		*args: Variable length argument list.
		**kwargs: Arbitrary keyword arguments.

	Returns:
		pd.DataFrame: File in python Pandas DataFrame Format.

	Raises:
		ValueError: If the number of columns is not `expected_n_columns`.

	Examples:
		chr	    var_source	    var_type	    var_start_pos	var_end_pos	    var_id
		X	    dbSNP	        SNV	2           4715204	        24715204	    rs1569277899
		15	    dbSNP	        SNV	            48411159	    48411159	    rs1555393532
		14	    dbSNP	        SNV	            73170963	    73170963	    rs63750599
		17	    dbSNP	        indel	        43091488	    43091489	    rs397509130
		X	    dbSNP	        indel	        38408871	    38408889	    rs72558434
		6	    dbSNP	        SNV	            24515243	    24515243	    rs375628463
		8	    dbSNP	        deletion	    89953453	    89953454	    rs1563526063
		X	    dbSNP	        SNV	            71223757	    71223757	    rs1555937009
		16	    dbSNP	        SNV	            79211665	    79211665	    rs587777127
		11	    dbSNP	        SNV	            35663574	    35663574	    rs886039241

	"""
	return read_file(file_path, expected_n_column)


def read_human_lof_variants(file_path, expected_n_column=6, *args, **kwargs):
	"""Read the file containing  a list of human loss-of-function (LoF) variants.

	Args:
		file_path (:obj:`str`): File path to be opened.
		expected_n_column (int): Number of expected columns of file (default=6).
		*args: Variable length argument list.
		**kwargs: Arbitrary keyword arguments.

	Returns:
		pd.DataFrame: File in python Pandas DataFrame Format.

	Raises:
		ValueError: If the number of columns is not 6.

	Examples:
		variant_id	    chromosome	variant_start_pos	variant_end_pos		variant_alleles		maf
		rs570863056	    8			120518051			120518051			C/A					0.000200
		rs139177747	    1			145873259			145873259			G/A/T				0.004593
		rs545016860	    2			1209214				1209214				G/A/T				0.000200
		rs552940002	    1			155205215			155205215			G/A/C/T				0.000200
		rs534927767	    17			10529463			10529463			G/A/C				0.000200
		rs143702633	    20			64237353			64237353			C/A/T				0.000200
		rs199975205	    6			158644697			158644697			G/A/C				0.001997
		rs566121317	    22			23780658			23780658			C/T					0.000200
		rs534474372	    20			53254073			53254073			C/A/T				0.000200
		rs183622528	    10			14908851			14908851			G/A/C				0.000599

	"""

	return read_file(file_path, expected_n_column)


if __name__ == '__main__':
	read_variants_associated_with_disease_predisposition("../data/raw/homo_sapiens_clinically_associated.txt")
	read_human_lof_variants("../data/raw/ccds_stop_gained.dat")

