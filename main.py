from src.io_utils import read_human_lof_variants, read_variants_associated_with_disease_predisposition
from src.intersections import join_overlapping, save_intersections_per_chromosome, calculate_maf
import click


@click.command()
@click.option('-d', '--disease-variants', default='data/raw/homo_sapiens_clinically_associated.txt',
              help='A list of human genetic variants previously associated with disease predisposition')
@click.option('-l', '--lof-variants', default='data/raw/ccds_stop_gained.dat',
              help='A list of human loss-of-function (LoF) variants')
@click.option('-o', '--output-dir-chromosome', default='data/processed/chromosome',
              help='Output dir to save the results of Task A (and to read for task B)')
@click.option('-om', '--output-dir-maf', default='data/processed/',
              help='Output dir to save the results of Task B')
@click.option('-s', '--save-maf', is_flag=True,
              help='Save the results (MAF) from Task B')
def run(disease_variants,
        lof_variants,
        output_dir_chromosome,
        output_dir_maf,
        save_maf,
        ):
	"""
	Usage: main.py [OPTIONS]

	Options:
	  -d, --disease-variants TEXT     A list of human genetic variants previously
	                                  associated with disease predisposition

	  -l, --lof-variants TEXT         A list of human loss-of-function (LoF)
	                                  variants

	  -o, --output-dir-chromosome TEXT
	                                  Output dir to save the results of Task A
	                                  (and to read for task B)

	  -om, --output-dir-maf TEXT      Output dir to save the results of Task B
	  -s, --save-maf                  Save the results (MAF) from Task B
	  --help                          Show this message and exit.
	"""
	save_intersections_per_chromosome(join_overlapping(
		read_variants_associated_with_disease_predisposition(disease_variants),
		read_human_lof_variants(lof_variants)
	), output_path=output_dir_chromosome
	)
	if save_maf:
		calculate_maf(folder_path=output_dir_chromosome, save_file=True, output_path=output_dir_maf)
	else:
		calculate_maf(folder_path=output_dir_chromosome, save_file=False)

if __name__ == '__main__':
	run()
